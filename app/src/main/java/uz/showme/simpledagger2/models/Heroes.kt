package uz.showme.simpledagger2.models

import com.google.gson.annotations.Expose

/**
 * Created by root on 3/10/18.
 */

class Heroes {

    @Expose
    var elements: List<Hero>? = null
}
