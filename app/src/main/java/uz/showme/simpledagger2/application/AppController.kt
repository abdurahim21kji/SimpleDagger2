package uz.showme.simpledagger2.application

import android.app.Application

import com.squareup.leakcanary.LeakCanary

import timber.log.Timber
import uz.showme.simpledagger2.BuildConfig
import uz.showme.simpledagger2.application.builder.AppComponent
import uz.showme.simpledagger2.application.builder.AppContextModule
import uz.showme.simpledagger2.application.builder.DaggerAppComponent

/**
 * Created by root on 3/10/18.
 */

class AppController : Application() {

    override fun onCreate() {
        super.onCreate()
        initialiseLogger()
        initAppComponent()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)

    }

    private fun initAppComponent() {
        netComponent = DaggerAppComponent.builder()
                .appContextModule(AppContextModule(this)).build()
    }

    private fun initialiseLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(object : Timber.Tree() {
                override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                    //TODO  decide what to log in release version
                }
            })
        }
    }

    companion object {

        var netComponent: AppComponent? = null
            private set
    }
}
