package uz.showme.simpledagger2.application.builder

import android.content.Context

import dagger.Module
import dagger.Provides

/**
 * Created by root on 3/10/18.
 */
@Module
class AppContextModule(private val context: Context) {

    @AppScope
    @Provides
    internal fun provideAppContext(): Context {
        return context
    }
}
