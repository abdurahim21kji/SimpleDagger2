package uz.showme.simpledagger2.application.builder

import android.content.Context

import java.io.File

import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import uz.showme.simpledagger2.utils.rx.AppRxSchedulers

/**
 * Created by root on 3/10/18.
 */

@Module
class NetworkModule {

    @AppScope
    @Provides
    internal fun provideHttpClient(interceptor: HttpLoggingInterceptor, cache: Cache): OkHttpClient {

        val builder = OkHttpClient.Builder()
        builder.addInterceptor(interceptor).cache(cache)
        return builder.build()

    }

    @AppScope
    @Provides
    internal fun provideInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor

    }

    @AppScope
    @Provides
    internal fun provideCacheFile(context: Context): File {
        return context.filesDir

    }

    @AppScope
    @Provides
    internal fun provideCache(file: File): Cache {
        return Cache(file, (10 * 10 * 1000).toLong())

    }

    @AppScope
    @Provides
    internal fun provideRxAdapter(): RxJavaCallAdapterFactory {
        return RxJavaCallAdapterFactory.createWithScheduler(AppRxSchedulers.INTERNET_SCHEDULERS)
    }

    @Provides
    internal fun provideGsonClient(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }
}
