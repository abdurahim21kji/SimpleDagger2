package uz.showme.simpledagger2.screen.detalis.heroes.core

import rx.Observable
import uz.showme.simpledagger2.api.HeroApi
import uz.showme.simpledagger2.models.Hero
import uz.showme.simpledagger2.models.Heroes
import uz.showme.simpledagger2.screen.detalis.heroes.HeroesListActivity
import uz.showme.simpledagger2.utils.NetworkUtils

/**
 * Created by root on 3/10/18.
 */

class HeroesModel( var context: HeroesListActivity,  var api: HeroApi) {
    val isNetworkAvialable: Observable<Boolean>
        get() = NetworkUtils.isNetworkAvailableObservable(context)

   fun provideListHeroes(): Observable<Heroes> {
        return api.heroes

    }

    fun gotoHeroDetailsActivity(hero: Hero) {
        context.goToHeroDetailsActivity(hero)
    }
}
