package uz.showme.simpledagger2.screen.detalis.heroes.list

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide

import butterknife.BindView
import butterknife.ButterKnife
import rx.subjects.PublishSubject
import uz.showme.simpledagger2.R
import uz.showme.simpledagger2.models.Hero

/**
 * Created by root on 3/10/18.
 */

class HeroViewHolder(internal var view: View, clickSubject: PublishSubject<Int>) : RecyclerView.ViewHolder(view) {

    @BindView(R.id.item_hero_image)
  var imageHero: ImageView? = null
    @BindView(R.id.item_hero_name)
    var nameHero: TextView? = null
    @BindView(R.id.item_hero_year)
    var dateReleaseHero: TextView? = null
    @BindView(R.id.item_hero_separator)
   var separatorHero: View? = null
    @BindView(R.id.item_hero_text)
   var descriptionHero: TextView? = null

    init {
        ButterKnife.bind(this, view)
        view.setOnClickListener { v -> clickSubject.onNext(adapterPosition) }
    }

    internal fun bind(hero: Hero) {
        Glide.with(view.context)
                .load(hero.image).into(imageHero!!)

        nameHero!!.text = if (TextUtils.isEmpty(hero.title)) "missing titile" else hero.title
        dateReleaseHero!!.text = if (TextUtils.isEmpty(hero.year)) "missing year" else hero.year
        descriptionHero!!.text = if (TextUtils.isEmpty(hero.intro)) "missing text" else hero.intro
        separatorHero!!.setBackgroundColor(Color.parseColor(hero.color))

    }

}
