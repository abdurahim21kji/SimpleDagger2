package uz.showme.simpledagger2.screen.detalis.dagger

import dagger.Module
import dagger.Provides
import uz.showme.simpledagger2.models.Hero
import uz.showme.simpledagger2.screen.detalis.HeroDetailsActivity
import uz.showme.simpledagger2.screen.detalis.HeroDetailsView

/**
 * Created by root on 3/10/18.
 */
@Module
class HeroDetailsModule(internal var detailsContext: HeroDetailsActivity, internal var hero: Hero) {

    @Provides
    internal fun view(): HeroDetailsView {
        return HeroDetailsView(detailsContext, hero)
    }

}
