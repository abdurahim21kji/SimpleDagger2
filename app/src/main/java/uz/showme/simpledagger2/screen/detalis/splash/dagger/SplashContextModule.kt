package uz.showme.simpledagger2.screen.detalis.splash.dagger

import dagger.Module
import dagger.Provides
import uz.showme.simpledagger2.screen.detalis.splash.SplashScreenActivity

/**
 * Created by root on 3/10/18.
 */
@Module
class SplashContextModule(internal var splashContext: SplashScreenActivity) {
    @SplashScope
    @Provides
    internal fun provideSplashContext(): SplashScreenActivity {
        return splashContext
    }
}
