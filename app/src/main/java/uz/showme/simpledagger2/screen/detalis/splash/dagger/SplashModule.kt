package uz.showme.simpledagger2.screen.detalis.splash.dagger

import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription
import uz.showme.simpledagger2.api.HeroApi
import uz.showme.simpledagger2.screen.detalis.splash.SplashScreenActivity
import uz.showme.simpledagger2.screen.detalis.splash.core.SplashModel
import uz.showme.simpledagger2.screen.detalis.splash.core.SplashPresenter
import uz.showme.simpledagger2.screen.detalis.splash.core.SplashView
import uz.showme.simpledagger2.utils.rx.RxSchedulers

/**
 * Created by root on 3/10/18.
 */
@Module
class SplashModule {

    @SplashScope
    @Provides
    internal fun providePresenter(schedulers: RxSchedulers, model: SplashModel): SplashPresenter {
        val compositeSubscription = CompositeSubscription()
        return SplashPresenter(model, schedulers, compositeSubscription)
    }

    @SplashScope
    @Provides
    internal fun provideSplash(context: SplashScreenActivity): SplashView {
        return SplashView(context)

    }

    @SplashScope
    @Provides
    internal fun provideSplashModel(api: HeroApi, contex: SplashScreenActivity): SplashModel {
        return SplashModel(api, contex)

    }
}
