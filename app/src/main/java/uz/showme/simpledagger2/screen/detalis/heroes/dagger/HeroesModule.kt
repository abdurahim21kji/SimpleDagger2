package uz.showme.simpledagger2.screen.detalis.heroes.dagger

import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription
import uz.showme.simpledagger2.api.HeroApi
import uz.showme.simpledagger2.screen.detalis.heroes.HeroesListActivity
import uz.showme.simpledagger2.screen.detalis.heroes.core.HeroesModel
import uz.showme.simpledagger2.screen.detalis.heroes.core.HeroesPresenter
import uz.showme.simpledagger2.screen.detalis.heroes.core.HeroesView
import uz.showme.simpledagger2.utils.rx.RxSchedulers

/**
 * Created by root on 3/10/18.
 */
@Module
class HeroesModule(internal var heroesListContext: HeroesListActivity) {

    @HeroesScope
    @Provides
    internal fun provideView(): HeroesView {
        return HeroesView(heroesListContext)

    }

    @HeroesScope
    @Provides
    internal fun providePresenter(schedulers: RxSchedulers, view: HeroesView, model: HeroesModel): HeroesPresenter {
        val subscription = CompositeSubscription()
        return HeroesPresenter(schedulers, model, view, subscription)
    }

    @HeroesScope
    @Provides
    internal fun provideContext(): HeroesListActivity {
        return heroesListContext

    }

    @HeroesScope
    @Provides
    internal fun provideModel(api: HeroApi): HeroesModel {
        return HeroesModel(heroesListContext, api)

    }
}
