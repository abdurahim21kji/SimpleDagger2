package uz.showme.simpledagger2.screen.detalis.heroes

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import java.io.Serializable

import javax.inject.Inject

import uz.showme.simpledagger2.application.AppController
import uz.showme.simpledagger2.models.Hero
import uz.showme.simpledagger2.screen.detalis.HeroDetailsActivity
import uz.showme.simpledagger2.screen.detalis.heroes.core.HeroesPresenter
import uz.showme.simpledagger2.screen.detalis.heroes.core.HeroesView
import uz.showme.simpledagger2.screen.detalis.heroes.dagger.DaggerHereosComponent
import uz.showme.simpledagger2.screen.detalis.heroes.dagger.HeroesModule

class HeroesListActivity : AppCompatActivity() {
    @Inject
    internal var view: HeroesView? = null
    @Inject
    internal var presenter: HeroesPresenter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerHereosComponent.builder().appComponent(AppController.getNetComponent())
                .heroesModule(HeroesModule(this)).build().inject(this)
        setContentView(view!!.view())
        presenter!!.onCreate()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter!!.onDestroy()
    }

    fun goToHeroDetailsActivity(hero: Hero) {

        val `in` = Intent(this, HeroDetailsActivity::class.java)
        `in`.putExtra("hero", hero as Serializable)
        startActivity(`in`)

    }
}
