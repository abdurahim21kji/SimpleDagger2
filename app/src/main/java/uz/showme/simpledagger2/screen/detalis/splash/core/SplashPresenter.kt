package uz.showme.simpledagger2.screen.detalis.splash.core

import android.util.Log

import rx.Subscription
import rx.subscriptions.CompositeSubscription
import uz.showme.simpledagger2.utils.UiUtils
import uz.showme.simpledagger2.utils.rx.RxSchedulers

/**
 * Created by root on 3/10/18.
 */

class SplashPresenter(private val model: SplashModel, private val rxSchedulers: RxSchedulers, private val subscriptions: CompositeSubscription) {

    private val heroesList: Subscription
        get() = model.isNetworkAvailable().doOnNext({ networkAvailable ->
            if (!networkAvailable) {
                Log.d("no conn", "no connexion")
            }
        }).filter({ isNetworkAvailable -> true }).flatMap({ isAvailable -> model.isNetworkAvailable() }).subscribeOn(rxSchedulers.internet()).observeOn(rxSchedulers.androidThread())
                .subscribe({ aBoolean -> model.gotoHeroesListActivity() },
                        { throwable -> UiUtils.handleThrowable(throwable) })

    fun onCreate() {
        subscriptions.add(heroesList)
    }

    fun onDestroy() {
        subscriptions.clear()
    }
}
