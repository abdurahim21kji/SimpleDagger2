package uz.showme.simpledagger2.screen.detalis.splash.core

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

import butterknife.ButterKnife
import uz.showme.simpledagger2.R
import uz.showme.simpledagger2.screen.detalis.splash.SplashScreenActivity

/**
 * Created by root on 3/10/18.
 */

class SplashView(context: SplashScreenActivity) {

    private val view: View

    init {
        val parent = FrameLayout(context)
        parent.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        view = LayoutInflater.from(context).inflate(R.layout.activity_splash_screen, parent, true)
        ButterKnife.bind(view, context)
    }

    fun constructView(): View {
        return view
    }
}
