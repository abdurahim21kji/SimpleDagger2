package uz.showme.simpledagger2.screen.detalis.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

import javax.inject.Inject

import uz.showme.simpledagger2.application.AppController
import uz.showme.simpledagger2.screen.detalis.heroes.HeroesListActivity
import uz.showme.simpledagger2.screen.detalis.splash.core.SplashPresenter
import uz.showme.simpledagger2.screen.detalis.splash.core.SplashView
import uz.showme.simpledagger2.screen.detalis.splash.dagger.DaggerSplashComponent
import uz.showme.simpledagger2.screen.detalis.splash.dagger.SplashContextModule

class SplashScreenActivity : AppCompatActivity() {


    @Inject
    internal var view: SplashView? = null
    @Inject
    internal var splashPresenter: SplashPresenter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerSplashComponent.builder().appComponent(AppController.getNetComponent()).splashContextModule(SplashContextModule(this)).build().inject(this)

        setContentView(view!!.constructView())
        splashPresenter!!.onCreate()

    }

    override fun onDestroy() {
        super.onDestroy()
        splashPresenter!!.onDestroy()
    }

    fun showHeroesListActivity() {
        Log.d("loaded", "ok showed")
        val i = Intent(this, HeroesListActivity::class.java)
        startActivity(i)
    }
}
