package uz.showme.simpledagger2.screen.detalis

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import javax.inject.Inject

import uz.showme.simpledagger2.models.Hero
import uz.showme.simpledagger2.screen.detalis.dagger.DaggerHeroDetailsComponent
import uz.showme.simpledagger2.screen.detalis.dagger.HeroDetailsModule

class HeroDetailsActivity : AppCompatActivity() {

    @Inject
    internal var view: HeroDetailsView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val hero = intent.extras.get("hero") as Hero
        DaggerHeroDetailsComponent.builder()
                .heroDetailsModule(HeroDetailsModule(this, hero))
                .build().inject(this)


        setContentView(view!!.view())
    }
}
