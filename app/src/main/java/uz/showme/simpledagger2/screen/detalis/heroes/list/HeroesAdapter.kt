package uz.showme.simpledagger2.screen.detalis.heroes.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import java.util.ArrayList

import rx.Observable
import rx.subjects.PublishSubject
import uz.showme.simpledagger2.R
import uz.showme.simpledagger2.models.Hero

/**
 * Created by root on 3/10/18.
 */

class HeroesAdapter : RecyclerView.Adapter<HeroViewHolder>() {

    private val itemClicks = PublishSubject.create<Int>()
    internal var listHeroes: ArrayList<Hero>? = ArrayList()

    fun swapAdapter(heroes: ArrayList<Hero>) {
        this.listHeroes!!.clear()
        this.listHeroes!!.addAll(heroes)
        notifyDataSetChanged()
    }

    fun observeClicks(): Observable<Int> {
        return itemClicks
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_hero, parent, false)
        return HeroViewHolder(view, itemClicks)
    }

    override fun onBindViewHolder(holder: HeroViewHolder, position: Int) {
        val hero = listHeroes!![position]
        holder.bind(hero)

    }

    override fun getItemCount(): Int {
        return if (listHeroes != null && listHeroes!!.size > 0) {
            listHeroes!!.size
        } else {

            0
        }
    }
}
