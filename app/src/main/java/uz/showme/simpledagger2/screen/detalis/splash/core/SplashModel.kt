package uz.showme.simpledagger2.screen.detalis.splash.core

import rx.Observable
import uz.showme.simpledagger2.api.HeroApi
import uz.showme.simpledagger2.models.Heroes
import uz.showme.simpledagger2.screen.detalis.splash.SplashScreenActivity
import uz.showme.simpledagger2.utils.NetworkUtils

/**
 * Created by root on 3/10/18.
 */

class SplashModel(private val api: HeroApi, private val splashContext: SplashScreenActivity) {

    internal val isNetworkAvailable: Observable<Boolean>
        get() = NetworkUtils.isNetworkAvailableObservable(splashContext)

    internal fun provideListHeroes(): Observable<Heroes> {
        return api.heroes
    }


    fun gotoHeroesListActivity() {
        splashContext.showHeroesListActivity()

    }

}
