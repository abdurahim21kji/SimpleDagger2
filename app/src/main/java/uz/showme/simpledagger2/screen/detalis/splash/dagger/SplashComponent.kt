package uz.showme.simpledagger2.screen.detalis.splash.dagger

import dagger.Component
import uz.showme.simpledagger2.application.builder.AppComponent
import uz.showme.simpledagger2.screen.detalis.splash.SplashScreenActivity

/**
 * Created by root on 3/10/18.
 */
@SplashScope
@Component(modules = arrayOf(SplashContextModule::class, SplashModule::class), dependencies = arrayOf(AppComponent::class))
interface SplashComponent {
    fun inject(activity: SplashScreenActivity)
}
