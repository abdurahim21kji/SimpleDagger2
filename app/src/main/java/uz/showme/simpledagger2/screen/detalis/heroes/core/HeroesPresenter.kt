package uz.showme.simpledagger2.screen.detalis.heroes.core

import android.util.Log

import java.util.ArrayList

import rx.Subscription
import rx.subscriptions.CompositeSubscription
import uz.showme.simpledagger2.models.Hero
import uz.showme.simpledagger2.utils.UiUtils
import uz.showme.simpledagger2.utils.rx.RxSchedulers

/**
 * Created by root on 3/10/18.
 */

class HeroesPresenter( var rxSchedulers: RxSchedulers, var model: HeroesModel, var view: HeroesView,  var subscriptions: CompositeSubscription) {
    var heros = ArrayList<Hero>()

    private val heroesList: Subscription
        get() {
            return model.isNetworkAvialable.doOnNext({ networkAvailable ->
                if (!networkAvailable) {
                    Log.v("no conn", "No connection")

                }
            }).filter({ isNetworkAvialable -> true })
                    .flatMap({ isAvailable -> model.provideListHeroes() })
                    .subscribeOn(rxSchedulers.internet())
                    .observeOn(rxSchedulers.androidThread()).subscribe({ heroes ->
                Log.v("ok loaded", "ccc")
                view.swapAdapter(heroes.elements as ArrayList<Hero>)
                heros = heroes.elements as ArrayList<Hero>
            }, ({ UiUtils.handleThrowable(throwable = Throwable()) }))
        }

    fun onCreate() {
        Log.d("enter to presenter", "oki")
        subscriptions.add(heroesList)
        subscriptions.add(respondToClick())
    }

    fun onDestroy() {
        subscriptions.clear()
    }

    private fun respondToClick(): Subscription {

        return view.itemClicks().subscribe { integer -> model.gotoHeroDetailsActivity(heros[integer!!]) }
    }
}
