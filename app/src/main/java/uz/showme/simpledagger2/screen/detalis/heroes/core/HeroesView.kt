package uz.showme.simpledagger2.screen.detalis.heroes.core

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

import java.util.ArrayList

import butterknife.BindView
import butterknife.ButterKnife
import rx.Observable
import uz.showme.simpledagger2.R
import uz.showme.simpledagger2.models.Hero
import uz.showme.simpledagger2.screen.detalis.heroes.HeroesListActivity
import uz.showme.simpledagger2.screen.detalis.heroes.list.HeroesAdapter

/**
 * Created by root on 3/10/18.
 */

class HeroesView(context: HeroesListActivity) {
    @BindView(R.id.activity_heroes_list_recycleview)
    internal var list: RecyclerView? = null

    internal var view: View
    internal var adapter: HeroesAdapter

    init {
        val parent = FrameLayout(context)
        parent.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        view = LayoutInflater.from(context).inflate(R.layout.activity_heroes_list, parent, true)
        ButterKnife.bind(this, view)

        adapter = HeroesAdapter()
        list!!.adapter = adapter
        val mLayoutManager = LinearLayoutManager(context)
        list!!.layoutManager = mLayoutManager

    }


    fun itemClicks(): Observable<Int> {
        return adapter.observeClicks()
    }

    fun swapAdapter(heroes: ArrayList<Hero>) {
        adapter.swapAdapter(heroes)
    }

    fun view(): View {
        return view
    }
}
