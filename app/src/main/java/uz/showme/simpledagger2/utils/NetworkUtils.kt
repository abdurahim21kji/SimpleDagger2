package uz.showme.simpledagger2.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

import rx.Observable

/**
 * Created by root on 3/10/18.
 */

object NetworkUtils {
    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    fun isNetworkAvailableObservable(context: Context): Observable<Boolean> {
        return Observable.just(NetworkUtils.isNetworkAvailable(context))
    }
}
