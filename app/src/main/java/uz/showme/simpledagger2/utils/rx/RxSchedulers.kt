package uz.showme.simpledagger2.utils.rx

import rx.Scheduler

/**
 * Created by root on 3/10/18.
 */

interface RxSchedulers {

    fun runOnBackground(): Scheduler

    fun io(): Scheduler

    fun compute(): Scheduler

    fun androidThread(): Scheduler

    fun internet(): Scheduler
}
