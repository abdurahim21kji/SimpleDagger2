package uz.showme.simpledagger2.utils

import android.support.design.widget.Snackbar
import android.view.View

import timber.log.Timber

/**
 * Created by root on 3/10/18.
 */

object UiUtils {

    fun handleThrowable(throwable: Throwable) {
        Timber.e(throwable, throwable.toString())
    }

    fun showSnackbar(view: View, message: String, length: Int) {
        Snackbar.make(view, message, length).setAction("Action", null).show()
    }
}
